//
//  MeetingUseCase.swift
//  EMedibox-iPad
//
//  Created by NLMBP0003 on 05/10/2021.
//  Copyright © 2021 NeoLab-VN. All rights reserved.
//

import Foundation
import RxSwift

protocol MeetingUseCase {
    func joinMeeting(reservationId: String) -> Observable<Result<CreateJoinInfo, Error>>
}

class DefaultMeetingUseCase: MeetingUseCase {

    // TODO: Implement by network service's app
    func joinMeeting(reservationId: String) -> Observable<Result<CreateJoinInfo, Error>> {
        return .never()
    }
}

// Use localhost to test
class LocalhostMeetingUseCase: MeetingUseCase {
    let meetingId = "room002"
    let name = "Tiep Nguyen iPad"

    func joinMeeting(reservationId: String) -> Observable<Result<CreateJoinInfo, Error>> {
        return .create { signal in
            JoinRequestService.postJoinInfoRequest(meetingId: self.meetingId, name: self.name) { (createJoinInfo, error)  in
                if let createJoinInfo = createJoinInfo {
                    signal.onNext(.success(createJoinInfo))
                } else if let error = error {
                    signal.onNext(.failure(error))
                }
                signal.onCompleted()
            }
            return Disposables.create()
        }
    }
}

//
//  ViewController.swift
//  DemoChimeSDK
//
//  Created by Tiep Nguyen on 26/10/2021.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func didTapJoin(_ sender: Any) {
        let viewModel = MeetingViewModel(reservationId: "")
        let meetingViewController = MeetingViewController(viewModel: viewModel)
        meetingViewController.modalPresentationStyle = .fullScreen
        present(meetingViewController, animated: true, completion: .none)
    }
}


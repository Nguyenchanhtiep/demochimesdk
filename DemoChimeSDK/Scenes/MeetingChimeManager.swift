//
//  MeetingChimeManager.swift
//  DemoChimeSDK
//
//  Created by Tiep Nguyen on 26/10/2021.
//

import UIKit
import AVFoundation
import AmazonChimeSDK

protocol MeetingChimeManagerDelegate: AnyObject {
    func localVideoDidAdd(userId: String, streamId: Int)
    func remoteVideoDidAdd(userId: String, streamId: Int)
    func localVideoDidRemove(userId: String, streamId: Int)
    func remoteVideoDidRemove(userId: String, streamId: Int)
    func usersJoinedDidChange(users: [MeetingChimeUser])
}

enum MeetingChimeSignalStrength {
    case none
    case low
    case high
}

class MeetingChimeUser {
    var name: String
    let userId: String
    var streamId: Int?
    let isLocal: Bool
    var signalStrength: MeetingChimeSignalStrength = .none {
        didSet {
            updateSignalStrengthHandler?(signalStrength)
            updateSignalStrengthHandlers.forEach { updateSignalStrengthHandler in
                updateSignalStrengthHandler(signalStrength)
            }
        }
    }
    var updateSignalStrengthHandler: ((MeetingChimeSignalStrength) -> Void)?
    var updateSignalStrengthHandlers: [((MeetingChimeSignalStrength) -> Void)] = []

    init(userId: String, name: String, isLocal: Bool, streamId: Int? = nil) {
        self.name = name
        self.userId = userId
        self.streamId = streamId
        self.isLocal = isLocal
    }
}

extension MeetingChimeUser: Equatable {
    static func ==(lhs: MeetingChimeUser, rhs: MeetingChimeUser) -> Bool {
        return lhs.userId == rhs.userId
    }
}
extension MeetingChimeUser: CustomStringConvertible {
    var description: String {
        return "UserId: \(userId), name: \(name), isLocal: \(isLocal)"
    }
}

class MeetingChimeManager {
    
    var currentMeetingSession: DefaultMeetingSession!
    var localUserId: String {
        currentMeetingSession.configuration.credentials.attendeeId
    }
    weak var delegate: MeetingChimeManagerDelegate?
    private let logger = ConsoleLogger(name: "MeetingChimeManager")
    var usersJoined: [MeetingChimeUser] = []

    init(joinInfo: CreateJoinInfo, delegate: MeetingChimeManagerDelegate?) {
        self.delegate = delegate
        let meetingResp = self.getCreateMeetingResponse(from: joinInfo)
        let attendeeResp = self.getCreateAttendeeResponse(from: joinInfo)
        let meetingSessionConfig = MeetingSessionConfiguration(createMeetingResponse: meetingResp,
                                                               createAttendeeResponse: attendeeResp)
        currentMeetingSession = DefaultMeetingSession(configuration: meetingSessionConfig, logger: logger)
    }

    func startMeeting() {
        self.configureAudioSession()
        self.startAudioVideoConnection()
        try? self.currentMeetingSession.audioVideo.startLocalVideo()
        self.currentMeetingSession.audioVideo.startRemoteVideo()
        self.stopVideoWhenTerminate()
    }

    @objc func stopMeeting() {
        usersJoined.forEach { user in
            if let streamId = user.streamId {
                currentMeetingSession.audioVideo.unbindVideoView(tileId: streamId)
            }
        }
        usersJoined.removeAll()
        currentMeetingSession.audioVideo.stopLocalVideo()
        currentMeetingSession.audioVideo.stopRemoteVideo()
        currentMeetingSession.audioVideo.stop()
    }

    func switchCamera() {
        currentMeetingSession.audioVideo.switchCamera()
    }

    func bindVideoView(videoView: VideoRenderView, id: Int) {
        currentMeetingSession.audioVideo.bindVideoView(videoView: videoView, tileId: id)
    }

    func unbindVideoView(id: Int) {
        currentMeetingSession.audioVideo.unbindVideoView(tileId: id)
    }
}

// MARK: - Config Default Meeting Session
extension MeetingChimeManager {

    private func getCreateMeetingResponse(from joinMeetingResponse: CreateJoinInfo) -> CreateMeetingResponse {
        let meeting = joinMeetingResponse.meeting.meeting
        let meetingResp = CreateMeetingResponse(
            meeting:
                Meeting(
                    externalMeetingId: meeting.externalMeetingId,
                    mediaPlacement: MediaPlacement(
                        audioFallbackUrl: meeting.mediaPlacement.audioFallbackUrl,
                        audioHostUrl: meeting.mediaPlacement.audioHostUrl,
                        signalingUrl: meeting.mediaPlacement.signalingUrl,
                        turnControlUrl: meeting.mediaPlacement.turnControlUrl,
                        eventIngestionUrl: meeting.mediaPlacement.eventIngestionUrl
                    ),
                    mediaRegion: meeting.mediaRegion,
                    meetingId: meeting.meetingId
                )
        )
        return meetingResp
    }

    private func getCreateAttendeeResponse(from joinMeetingResponse: CreateJoinInfo) -> CreateAttendeeResponse {
        let attendee = joinMeetingResponse.attendee.attendee
        let attendeeResp = CreateAttendeeResponse(
            attendee:
                Attendee(attendeeId: attendee.attendeeId,
                         externalUserId: attendee.externalUserId,
                         joinToken: attendee.joinToken)
        )
        return attendeeResp
    }
}

// MARK: - Handle Start Meeting
extension MeetingChimeManager {

    func configureAudioSession() {
        let audioSession = AVAudioSession.sharedInstance()
        do {
            if audioSession.category != .playAndRecord {
                try audioSession.setCategory(AVAudioSession.Category.playAndRecord,
                                             options: AVAudioSession.CategoryOptions.allowBluetooth)
                try audioSession.setActive(true, options: .notifyOthersOnDeactivation)
            }
            if audioSession.mode != .voiceChat {
                try audioSession.setMode(.voiceChat)
            }
        } catch {
            print("Error configuring AVAudioSession: \(error.localizedDescription)")
        }
    }

    private func startAudioVideoConnection() {
        setupAudioVideoFacadeObservers()
        try? currentMeetingSession.audioVideo.start()
    }

    private func setupAudioVideoFacadeObservers() {
        let audioVideo = currentMeetingSession.audioVideo
        audioVideo.addVideoTileObserver(observer: self)
        audioVideo.addRealtimeObserver(observer: self)
        audioVideo.addAudioVideoObserver(observer: self)
    }
}

extension MeetingChimeManager: VideoTileObserver {

    func videoTileDidAdd(tileState: VideoTileState) {
        print("VideoTileObserver videoTileDidAdd \(tileState.tileId)")
        if tileState.isLocalTile {
            delegate?.localVideoDidAdd(userId: tileState.attendeeId, streamId: tileState.tileId)
        } else {
            delegate?.remoteVideoDidAdd(userId: tileState.attendeeId, streamId: tileState.tileId)
        }
    }

    func videoTileDidRemove(tileState: VideoTileState) {
        print("VideoTileObserver videoTileDidRemove")
        if tileState.isLocalTile {
            delegate?.localVideoDidRemove(userId: tileState.attendeeId, streamId: tileState.tileId)
        } else {
            delegate?.remoteVideoDidRemove(userId: tileState.attendeeId, streamId: tileState.tileId)
        }
    }

    func videoTileDidPause(tileState: VideoTileState) {
        print("VideoTileObserver videoTileDidPause")
    }

    func videoTileDidResume(tileState: VideoTileState) {
        print("VideoTileObserver videoTileDidResume")
    }

    func videoTileSizeDidChange(tileState: VideoTileState) {
        print("VideoTileObserver videoTileSizeDidChange")
    }
}

extension MeetingChimeManager: AudioVideoObserver {

    func audioSessionDidStartConnecting(reconnecting: Bool) {
    }

    func audioSessionDidStart(reconnecting: Bool) {
    }

    func audioSessionDidDrop() {
    }

    func audioSessionDidStopWithStatus(sessionStatus: MeetingSessionStatus) {
    }

    func audioSessionDidCancelReconnect() {
    }

    func connectionDidRecover() {
    }

    func connectionDidBecomePoor() {
    }

    func videoSessionDidStartConnecting() {
    }

    func videoSessionDidStartWithStatus(sessionStatus: MeetingSessionStatus) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            let user = self.usersJoined.first(where: { $0.isLocal })
            switch sessionStatus.statusCode {
            case .ok:
                user?.signalStrength = .high
            case .networkBecomePoor:
                user?.signalStrength = .low
            default:
                user?.signalStrength = .none
            }
        }
    }

    func videoSessionDidStopWithStatus(sessionStatus: MeetingSessionStatus) {
    }
}

extension MeetingChimeManager: RealtimeObserver {

    func volumeDidChange(volumeUpdates: [VolumeUpdate]) {
        print("RealtimeObserver volumeDidChange")
    }

    func signalStrengthDidChange(signalUpdates: [SignalUpdate]) {
        print("RealtimeObserver signalStrengthDidChange \(signalUpdates)")
        signalUpdates.forEach { signalUpdate in
            let attendeeId = signalUpdate.attendeeInfo.attendeeId
            let user = usersJoined.first(where: { $0.userId == attendeeId })
            switch signalUpdate.signalStrength {
            case .none:
                user?.signalStrength = .none
            case .low:
                user?.signalStrength = .low
            case .high:
                user?.signalStrength = .high
            @unknown default:
                user?.signalStrength = .none
            }
        }
    }

    func attendeesDidJoin(attendeeInfo: [AttendeeInfo]) {
        var newUsersJoin = attendeeInfo.map({
            MeetingChimeUser(userId: $0.attendeeId,
                             name: convertAttendeeName(from: $0),
                             isLocal: $0.attendeeId == localUserId)
        })
        newUsersJoin.sort(by: { $0.userId == localUserId || $1.userId != localUserId })
        usersJoined.append(contentsOf: newUsersJoin)
        delegate?.usersJoinedDidChange(users: usersJoined)
        print("RealtimeObserver attendeesDidJoin \(usersJoined)")
    }

    func attendeesDidLeave(attendeeInfo: [AttendeeInfo]) {
        let leaveUserIds = attendeeInfo.map({ $0.attendeeId })
        usersJoined.removeAll(where: { leaveUserIds.contains($0.userId) })
        delegate?.usersJoinedDidChange(users: usersJoined)
        print("RealtimeObserver attendeesDidLeave \(usersJoined)")
    }

    func attendeesDidDrop(attendeeInfo: [AttendeeInfo]) {
        print("RealtimeObserver attendeesDidDrop")
    }

    func attendeesDidMute(attendeeInfo: [AttendeeInfo]) {
        print("RealtimeObserver attendeesDidMute")
    }

    func attendeesDidUnmute(attendeeInfo: [AttendeeInfo]) {
        print("RealtimeObserver attendeesDidUnmute")
    }

    func convertAttendeeName(from info: AttendeeInfo) -> String {
        let externalUserIdArray = info.externalUserId.components(separatedBy: "#")
        return externalUserIdArray.last ?? ""
    }
}

extension MeetingChimeManager {
    func stopVideoWhenTerminate() {
        NotificationCenter.default.addObserver(self, selector: #selector(stopMeeting), name: UIApplication.willTerminateNotification, object: nil)
    }
}

//
//  UserJoinedTableViewCell.swift
//  DemoChimeSDK
//
//  Created by Tiep Nguyen on 26/10/2021.
//

import UIKit

class UserJoinedTableViewCell: UITableViewCell {

    @IBOutlet private weak var avatarImageView: UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var signalQualityImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setup(user: MeetingChimeUser) {
        self.nameLabel.text = user.name
        signalQualityImageView.image = user.signalStrength.image
        user.updateSignalStrengthHandler = { [weak self] signalStrength in
            self?.signalQualityImageView.image = signalStrength.image
        }
    }
}

//
//  MeetingViewModel.swift
//  EMedibox-iPad
//
//  Created by NLMBP0003 on 05/10/2021.
//  Copyright © 2021 NeoLab-VN. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import AmazonChimeSDK

extension Result {
    func error() -> Failure? {
        switch self {
            case .success:
                return nil
            case .failure(let error):
                return error
        }
    }
}

class MeetingViewModel: ViewModelTransformable {
    
    private var reservationId: String
    let permissionRequester: PermissionRequestable = DefaultPermissionRequester()
    var meetingChimeManager: MeetingChimeManager?
    var usersJoined: [MeetingChimeUser] = []
    var indicatorStatus = BehaviorSubject<IndicatorStatus>(value: .hide)
    let error = PublishSubject<String>()
    var localVideoDidAdd = PublishSubject<Int>()
    var remoteVideoDidAdd = PublishSubject<Int>()
    var localVideoDidRemove = PublishSubject<Int>()
    var remoteVideoDidRemove = PublishSubject<Int>()

    var localVideoViewType = BehaviorSubject<LocalVideoViewType>(value: .fullScreen)
    var hiddenCloseFullscreen = BehaviorSubject<Bool>(value: true)
    var hiddenShowFullscreen = BehaviorSubject<Bool>(value: true)
    var hiddenSwitchCamera = BehaviorSubject<Bool>(value: true)
    var hiddenLocalSignalStrength = BehaviorSubject<Bool>(value: true)

    var usersJoinedSignal = BehaviorSubject<[MeetingChimeUser]>(value: [])

    var endCall = PublishSubject<Void>()

    private let disposeBag = DisposeBag()

    init(reservationId: String) {
        self.reservationId = reservationId
    }

    func transform(input: Input) -> Output {
        let requestMediaPermission = self.requestMediaPermission().share(replay: 1)
        let requestMediaPermissionSuccess = requestMediaPermission.compactMap({ try? $0.get() })
        let requestMediaPermissionError = requestMediaPermission.compactMap({ $0.error()?.description})

        let requestJoinMeeting = input.viewWillAppear
            .flatMap({ _ in requestMediaPermissionSuccess })
            .flatMapLatest({
                LocalhostMeetingUseCase().joinMeeting(reservationId: self.reservationId)
//                DefaultMeetingUseCase().joinMeeting(reservationId: self.reservationId)
            })
            .share()
        
        let requestJoinMeetingSuccess = requestJoinMeeting
            .compactMap({try? $0.get()})
        createMeetingSessionConfiguration(joinInfoObservable: requestJoinMeetingSuccess)
        
        let requestJoinMeetingFail = requestJoinMeeting.compactMap({ (result) -> Error? in
            if case .failure(let error) = result {
                return error
            }
            return nil
        }).map({ $0.localizedDescription })
        
        let error = Observable.merge(requestMediaPermissionError, requestJoinMeetingFail)
            .asDriverOnErrorJustComplete()

        handleTapSwitchCamera(trigger: input.didTapSwitchCamera)
        handleLocalVideoView(didTapShowFullscreenLocalVideo: input.didTapShowFullscreenLocalVideo,
                             didTapCloseFullscreenLocalVideo: input.didTapCloseFullscreenLocalVideo)
        handleTapEndCall(trigger: input.tapEndMeeting)
        return Output(indicatorStatus: indicatorStatus.asDriverOnErrorJustComplete(),
                      error: error,
                      localVideoDidAdd: localVideoDidAdd.asDriverOnErrorJustComplete(),
                      remoteVideoDidAdd: remoteVideoDidAdd.asDriverOnErrorJustComplete(),
                      localVideoDidRemove: localVideoDidRemove.asDriverOnErrorJustComplete(),
                      remoteVideoDidRemove: remoteVideoDidRemove.asDriverOnErrorJustComplete(),
                      localVideoViewType: localVideoViewType.asDriverOnErrorJustComplete(),
                      hiddenCloseFullscreen: hiddenCloseFullscreen.asDriverOnErrorJustComplete(),
                      hiddenShowFullscreen: hiddenShowFullscreen.asDriverOnErrorJustComplete(),
                      hiddenSwitchCamera: hiddenSwitchCamera.asDriverOnErrorJustComplete(),
                      hiddenLocalSignalStrength: hiddenLocalSignalStrength.asDriverOnErrorJustComplete(),
                      usersJoined: usersJoinedSignal.asDriverOnErrorJustComplete(),
                      endCall: endCall.asDriverOnErrorJustComplete())
    }

    private func requestMediaPermission() -> Observable<Result<Void, MediaPermissionError>> {
        return .create { signal in
            self.permissionRequester.requestMediaPermission { result in
                signal.onNext(result)
                signal.onCompleted()
            }
            return Disposables.create()
        }
    }

    private func createMeetingSessionConfiguration(joinInfoObservable: Observable<CreateJoinInfo>) {
        self.indicatorStatus.onNext(.show)
        joinInfoObservable.bind { [weak self] (joinInfo) in
            guard let self = self else { return }
            self.meetingChimeManager = MeetingChimeManager(joinInfo: joinInfo, delegate: self)
            self.meetingChimeManager?.startMeeting()
        }
        .disposed(by: disposeBag)
    }

    private func handleTapSwitchCamera(trigger: Observable<Void>) {
        trigger
            .subscribe(onNext: { [weak self] _ in
                self?.meetingChimeManager?.switchCamera()
            })
            .disposed(by: disposeBag)
    }

    private func handleLocalVideoView(didTapShowFullscreenLocalVideo: Observable<Void>,
                                      didTapCloseFullscreenLocalVideo: Observable<Void>) {
        didTapShowFullscreenLocalVideo
            .subscribe(onNext: { [weak self] _ in
                self?.localVideoViewType.onNext(.fullScreen)
                self?.hiddenCloseFullscreen.onNext(false)
                self?.hiddenShowFullscreen.onNext(true)
                self?.hiddenSwitchCamera.onNext(false)
                self?.hiddenLocalSignalStrength.onNext(true)
            }).disposed(by: disposeBag)
        didTapCloseFullscreenLocalVideo
            .subscribe(onNext: { [weak self] _ in
                self?.localVideoViewType.onNext(.mirror)
                self?.hiddenCloseFullscreen.onNext(true)
                self?.hiddenShowFullscreen.onNext(false)
                self?.hiddenSwitchCamera.onNext(true)
                self?.hiddenLocalSignalStrength.onNext(false)
            }).disposed(by: disposeBag)
    }

    private func handleTapEndCall(trigger: Observable<Void>) {
        trigger
            .subscribe(onNext: { [weak self] _ in
                self?.meetingChimeManager?.stopMeeting()
                self?.endCall.onNext(())
            })
            .disposed(by: disposeBag)
    }
}

extension MeetingViewModel {

    enum LocalVideoViewType {
        case mirror
        case fullScreen
    }

    enum IndicatorStatus {
        case show
        case hide
    }

    struct Input {
        var viewWillAppear: Observable<Void>
        var tapEndMeeting: Observable<Void>
        var didTapSwitchCamera: Observable<Void>
        var didTapShowFullscreenLocalVideo: Observable<Void>
        var didTapCloseFullscreenLocalVideo: Observable<Void>
    }
    
    struct Output {
        var indicatorStatus: Driver<IndicatorStatus>
        var error: Driver<String>
        var localVideoDidAdd: Driver<Int>
        var remoteVideoDidAdd: Driver<Int>
        var localVideoDidRemove: Driver<Int>
        var remoteVideoDidRemove: Driver<Int>

        var localVideoViewType: Driver<LocalVideoViewType>
        var hiddenCloseFullscreen: Driver<Bool>
        var hiddenShowFullscreen: Driver<Bool>
        var hiddenSwitchCamera: Driver<Bool>
        var hiddenLocalSignalStrength: Driver<Bool>

        var usersJoined: Driver<[MeetingChimeUser]>

        var endCall: Driver<Void>
    }
}

extension MeetingViewModel: MeetingChimeManagerDelegate {

    func localVideoDidAdd(userId: String, streamId: Int) {
        localVideoDidAdd.onNext(streamId)
    }

    func remoteVideoDidAdd(userId: String, streamId: Int) {
        remoteVideoDidAdd.onNext(streamId)
    }

    func localVideoDidRemove(userId: String, streamId: Int) {
        localVideoDidRemove.onNext(streamId)
    }

    func remoteVideoDidRemove(userId: String, streamId: Int) {
        remoteVideoDidRemove.onNext(streamId)
    }

    func usersJoinedDidChange(users: [MeetingChimeUser]) {
        if usersJoined.isEmpty && !users.isEmpty {
            self.indicatorStatus.onNext(.hide)
        }
        if usersJoined.count < users.count && users.count == 2 {
            self.localVideoViewType.onNext(.mirror)
            self.hiddenCloseFullscreen.onNext(true)
            self.hiddenShowFullscreen.onNext(false)
            self.hiddenSwitchCamera.onNext(true)
            self.hiddenLocalSignalStrength.onNext(false)
        }
        if users.count == 1 {
            self.localVideoViewType.onNext(.fullScreen)
            self.hiddenCloseFullscreen.onNext(true)
            self.hiddenShowFullscreen.onNext(true)
            self.hiddenSwitchCamera.onNext(false)
        }
        usersJoined = users
        usersJoinedSignal.onNext(usersJoined)
    }
}

//
//  MeetingViewController.swift
//  DemoChimeSDK
//
//  Created by Tiep Nguyen on 26/10/2021.
//

import UIKit
import RxSwift
import RxCocoa
import RxAppState
import AVFoundation
import AmazonChimeSDK
import SwifterSwift

class MeetingViewController: UIViewController {

    @IBOutlet weak var localVideoView: DefaultVideoRenderView!
    @IBOutlet weak var remoteVideoView: DefaultVideoRenderView!

//    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet var mirrorLocalVideoConstraints: [NSLayoutConstraint]!
    @IBOutlet var fullscreenLocalVideoConstraints: [NSLayoutConstraint]!
    @IBOutlet weak var closeFullScreenLocalVideoButton: UIButton!
    @IBOutlet weak var showFullscreenLocalVideoButton: UIButton!
    @IBOutlet weak var localSignalStrengthImageView: UIImageView!

    @IBOutlet weak var endButton: UIButton!
    @IBOutlet weak var switchCameraButton: UIButton!

    @IBOutlet weak var usersJoinContainerTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var usersJoinContainerWidthConstraint: NSLayoutConstraint!

    @IBOutlet weak var usersTableView: UITableView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!

    private var viewModel: MeetingViewModel
    private let disposeBag = DisposeBag()
    var endCall: () -> Void = {}

    init(viewModel: MeetingViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        bindingData()
    }
    @IBAction func didTapShowHideListUsers(_ sender: Any) {
        let isShowingListUserJoined = usersJoinContainerTrailingConstraint.constant == 0
        usersJoinContainerTrailingConstraint.constant = isShowingListUserJoined ? usersJoinContainerWidthConstraint.constant : 0
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }

    private func bindingData() {
        let input = MeetingViewModel.Input(viewWillAppear: rx.viewWillAppear.mapToVoid(),
                                           tapEndMeeting: endButton.rx.tap.asObservable(),
                                           didTapSwitchCamera: switchCameraButton.rx.tap.asObservable(),
                                           didTapShowFullscreenLocalVideo: showFullscreenLocalVideoButton.rx.tap.asObservable(),
                                           didTapCloseFullscreenLocalVideo: closeFullScreenLocalVideoButton.rx.tap.asObservable())
        let output = viewModel.transform(input: input)

        output.indicatorStatus
            .drive(onNext: { [weak self] status in
                switch status {
                    case .show:
                        self?.loadingIndicator?.startAnimating()
                    case .hide:
                        self?.loadingIndicator?.stopAnimating()
                }
            })
            .disposed(by: disposeBag)
        output.error.drive { [weak self] (error) in
            self?.loadingIndicator?.stopAnimating()
            // TODO: Need implement by business logic of app
            self?.showAlert(title: "", message: error)
        }
        .disposed(by: disposeBag)

        output.localVideoDidAdd
            .drive(onNext: { [weak self] id in
                guard let self = self else { return }
                self.viewModel.meetingChimeManager?.bindVideoView(videoView: self.localVideoView, id: id)
            })
            .disposed(by: disposeBag)
        output.remoteVideoDidAdd
            .drive(onNext: { [weak self] id in
                guard let self = self else { return }
                self.viewModel.meetingChimeManager?.bindVideoView(videoView: self.remoteVideoView, id: id)
            })
            .disposed(by: disposeBag)
        output.localVideoDidRemove
            .drive(onNext: { [weak self] id in
                self?.viewModel.meetingChimeManager?.unbindVideoView(id: id)
                self?.localVideoView.resetImage()
            })
            .disposed(by: disposeBag)

        output.remoteVideoDidRemove
            .drive(onNext: { [weak self] id in
                self?.viewModel.meetingChimeManager?.unbindVideoView(id: id)
                self?.remoteVideoView.resetImage()
            })
            .disposed(by: disposeBag)
        output.localVideoViewType
            .drive(onNext: { [weak self] type in
                guard let self = self else { return }
                switch type {
                    case .mirror:
                        self.mirrorLocalVideoConstraints.forEach({ $0.priority = .defaultHigh })
                        self.fullscreenLocalVideoConstraints.forEach({ $0.priority = .defaultLow })
                        self.localVideoView.layer.cornerRadius = 8
                        self.localVideoView.layer.borderWidth = 0.5
                    case .fullScreen:
                        self.mirrorLocalVideoConstraints.forEach({ $0.priority = .defaultLow })
                        self.fullscreenLocalVideoConstraints.forEach({ $0.priority = .defaultHigh })
                        self.localVideoView.layer.cornerRadius = 0
                        self.localVideoView.layer.borderWidth = 0
                }
            })
            .disposed(by: disposeBag)
        output.hiddenSwitchCamera
            .drive(switchCameraButton.rx.isHidden)
            .disposed(by: disposeBag)
        output.hiddenCloseFullscreen
            .drive(closeFullScreenLocalVideoButton.rx.isHidden)
            .disposed(by: disposeBag)
        output.hiddenShowFullscreen
            .drive(showFullscreenLocalVideoButton.rx.isHidden)
            .disposed(by: disposeBag)
        output.hiddenLocalSignalStrength
            .drive(localSignalStrengthImageView.rx.isHidden)
            .disposed(by: disposeBag)
        output.usersJoined
            .asObservable()
            .compactMap({ $0.first(where: { $0.isLocal }) })
            .take(1)
            .asDriverOnErrorJustComplete()
            .drive(onNext: { [weak self] user in
                self?.localSignalStrengthImageView.image = user.signalStrength.image
                let updateSignalStrengthHandler: ((MeetingChimeSignalStrength) -> Void) = { [weak self] signalStrength in
                    self?.localSignalStrengthImageView.image = signalStrength.image
                }
                user.updateSignalStrengthHandlers.append(updateSignalStrengthHandler)
            })
            .disposed(by: disposeBag)
        output.usersJoined
            .drive(onNext: { _ in
                self.usersTableView.reloadData()
            })
            .disposed(by: disposeBag)
        output.endCall
            .drive(onNext: { [weak self] _ in
                UIApplication.shared.isIdleTimerDisabled = false
                self?.dismiss(animated: true, completion: { [weak self] in
                    self?.endCall()
                })
            })
            .disposed(by: disposeBag)
    }

    private func setupUI() {
        UIApplication.shared.isIdleTimerDisabled = true
        localVideoView.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.6).cgColor
        localVideoView.backgroundColor = .white
        remoteVideoView.backgroundColor = .clear
        setupUsersTableView()
    }

    // TODO: Need implement by business logic of app
    private func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let acceptAction = UIAlertAction(title: "OK", style: .default, handler: .none)
        alert.addAction(acceptAction)
        present(alert, animated: true, completion: .none)
    }
}

extension MeetingViewController {

    func setupUsersTableView() {
        usersTableView.register(nibWithCellClass: UserJoinedTableViewCell.self)
        usersTableView.dataSource = self
        usersTableView.contentInset = UIEdgeInsets(top: 14, left: 0, bottom: 0, right: 0)
    }
}

extension MeetingViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.usersJoined.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let user = viewModel.usersJoined[indexPath.row]
        let cell = tableView.dequeueReusableCell(withClass: UserJoinedTableViewCell.self)
        cell.setup(user: user)
        return cell
    }
}

extension MeetingChimeSignalStrength {
    var image: UIImage? {
        switch self {
            case .high:
                return UIImage(named: "high_quality")
            case .low:
                return UIImage(named: "low_quality")
            case .none:
                return UIImage(named: "none_quality")
        }
    }
}

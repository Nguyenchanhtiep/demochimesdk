//
//  ViewModelTransformable.swift
//  DemoChimeSDK
//
//  Created by Tiep Nguyen on 26/10/2021.
//

import Foundation

public protocol ViewModelTransformable: class {
    associatedtype Input
    associatedtype Output
    func transform(input: Input) -> Output
}

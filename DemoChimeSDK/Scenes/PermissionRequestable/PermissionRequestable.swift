//
//  PermissionRequestable.swift
//  DemoChimeSDK
//
//  Created by Tiep Nguyen on 26/10/2021.
//

import Foundation
import AVFoundation

enum MediaPermissionError: Error {
    case audioDenied
    case videoDenied
}

extension MediaPermissionError: LocalizedError {
    var description: String {
        switch self {
        case .audioDenied:
            return "マイクを停止しました"
        case .videoDenied:
            return "カメラを停止しました"
        }
    }
}

protocol PermissionRequestable {
    func requestMediaPermission(completion: @escaping (Result<Void, MediaPermissionError>) -> Void)
}

class DefaultPermissionRequester: PermissionRequestable {

    func requestMediaPermission(completion: @escaping (Result<Void, MediaPermissionError>) -> Void) {
        requestAudioPermission { [weak self] result in
            switch result {
                case .success:
                    self?.requestCameraPermission { result in
                        switch result {
                            case .success:
                                completion(.success(()))
                            case .failure(let error):
                                completion(.failure(error))
                        }
                    }
                case .failure(let error):
                    completion(.failure(error))
            }
        }
    }

    private func requestAudioPermission(completion: @escaping (Result<Void, MediaPermissionError>) -> Void) {
        switch AVAudioSession.sharedInstance().recordPermission {
            case AVAudioSession.RecordPermission.granted:
                completion(.success(()))
            case AVAudioSession.RecordPermission.denied:
                completion(.failure(MediaPermissionError.audioDenied))
            case AVAudioSession.RecordPermission.undetermined:
                AVAudioSession.sharedInstance().requestRecordPermission({ (granted) in
                    if granted {
                        completion(.success(()))
                    } else {
                        completion(.failure(MediaPermissionError.audioDenied))
                    }
                })
            @unknown default:
                break
        }
    }

    private func requestCameraPermission(completion: @escaping (Result<Void, MediaPermissionError>) -> Void) {
        AVCaptureDevice.requestAccess(for: .video) { granted in
            if granted {
                completion(.success(()))
            } else {
                completion(.failure(MediaPermissionError.videoDenied))
            }
        }
    }
}
